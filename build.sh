#!/usr/bin/env bash

cd en/
haunt build
cd ../el
haunt build
cd ../
[ -d site/ ] && rm -r site/
mkdir site/
mv en/site/ site/en/
mv el/site site/el/
mv site/en/assets site/assets
cp site/en/index.html site/index.html 
