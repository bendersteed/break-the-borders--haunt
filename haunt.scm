(use-modules (haunt asset)
	     (haunt site)
	     (haunt page)
	     (haunt post)
	     (haunt html)
	     (haunt utils)
	     (haunt builder blog)
	     (haunt builder atom)
	     (haunt builder assets)
	     (haunt reader)
	     (haunt reader skribe))
;; theme

(define contact
  `(section (@ (class "hero-contact hero contact is-success"))
	    (div (@ (class "container"))
		 (h1 (@ (class "title"))
		     "Never hesitate to contact us!")
		 (div (@ (class "field"))
		      (label (@ (class "laber")) "From:")
		      (div (@ (class "field-body"))
			   (div (@ (class "field"))
				(p (@ (class "control is-expanded has-icons-left"))
				   (input (@ (class "input")
					     (type "text")
					     (placeholder "Name")))
				   (span (@ (class "icon is-small is-left"))
					 (i (@ (class "fas fa-user")))))))
		      (div (@ (class "field-body"))
			   (div (@ (class "field"))
				(p (@ (class "control is-expanded has-icons-left"))
				   (input (@ (class "input")
					     (type "email")
					     (placeholder "E-mail")))
				   (span (@ (class "icon is-small is-left"))
					 (i (@ (class "fas fa-envelope"))))))))
		 (div (@ (class "field"))
		      (label (@ (class "laber")) "Subject:")
		      (div (@ (class "field-body"))
			   (div (@ (class "field"))
				(div (@ (class "control"))
				     (input (@ (class "input")
					       (type "text")
					       (placeholder "Subject")))))))
		 (div (@ (class "field"))
		      (label (@ (class "laber")) "Message:")
		      (div (@ (class "field-body"))
			   (div (@ (class "field"))
				(div (@ (class "control"))
				     (textarea (@ (class "textarea")
						  (placeholder "Your message here!")))))))
		 (div (@ (class "field"))
		      (div (@ (class "field")))
		      (div (@ (class "field-body"))
			   (div (@ (class "control"))
				(button (@ (class "button is-primary"))
					"Send message")))))))

(define footer
  `(footer (@ (class "footer"))
	   (div (@ (class "columns"))
		(div (@ (class "column"))
		     (h1 "Connect with us")
		     (span (@ (class "icon is-large"))
			   (i (@ (class "fab fa-facebook-f"))))
		     (span (@ (class "icon is-large"))
			   (i (@ (class "fas fa-envelope")))))
		(div (@ (class "column"))
		     (p (@ (class "footer-middle"))
			(span (@ (class "icon is-large"))
			      (i (@ (class "far fa-copyright"))))
			"Break the Borders, 2019")
		     (p "All content is shared under the
Creative Commons Attribution-Sharealike 4.0 International license."))
		(div (@ (class "column footer-right"))
		     (p "Designed by "
			(a (@ (href "https://bendersteed.gitlab.io"))
			   "bendersteed") " with lot's of love.")
		     (p "Powered by "
			(a (@ (href "http://haunt.dthompson.us/"))
			   "Haunt") ".")))))


(define* (base-tmpl site body #:key title)
  `((doctype html)
    (head
     (meta (@ (charset "utf-8")))
     (meta (@ (name "viewport")
	      (content "width=device-width, initial-scale=1")))
     (title ,(if title
		 (string-append title " | Break the Borders")
		 (site-title site)))
     ;; css
     (link (@ (rel "stylesheet")
	      (href "https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css")))
     
     (link (@ (rel "stylesheet")
	      (href "https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap")))
     (link (@ (rel "stylesheet")
	      (href "/assets/btb.css")))
     ;; atom feed
     (link (@ (rel "alternate")
	      (title "ActivityPub news")
	      (type "application/atom+xml")
	      (href "/feed.xml")))
     (script (@ (defer "")
		(src "https://kit.fontawesome.com/aa442d8292.js")
		(crossorigin "anonymous"))))
    (body ,body
	  ,contact
	  ,footer)))

(define (hero-nav-post post)
  `(section (@ (class "hero-nav hero is-success is-small"))
	    (div (@ (class "hero-head"))
		 (nav (@ (class "navbar is-fixed"))
		      (div (@ (class "container"))
			   (div (@ (class "navbar-brand"))
				(a (@ (class "navbar-item")
				      (href  "https://breaktheborders.gr"))
				   (img (@ (src "/assets/img/btb.jpg")
					   (width "56") (height "28"))))
				(span (@ (class "navbar-burger burger")
					 (data-target "navbarMenuHeroA"))
				      (span)
				      (span)
				      (span)))
			   (div (@ (id "navbarMenuHeroA")
				   (class "navbar-menu"))
				(div (@ (class "navbar-end"))
				     (a (@ (class "navbar-item"))
					"Home")
				     (a (@ (class "navbar-item"))
					"About")
				     (a (@ (class "navbar-item"))
					"News")
				     (a (@ (class "navbar-item"))
					"Contact")))))
		 (div (@ (class "hero-body"))
		      (div (@ (class "container"))
			   (h1 (@ (class "title"))
			       ,(post-ref post 'title))
			   (h2 (@ (class "subtitle"))
			       ,(date->string* (post-date post))))))))
(define (post-template post)
  `(div ,(hero-nav-post post)
	(section (@ (class "article-content"))
		 (div (@ (class "container is-size-5"))
		      ,(post-sxml post)))))

(define btb-haunt-theme
  (theme #:name "Break the Borders"
	 #:layout
	 (lambda (site title body)
	   (base-tmpl
	    site body
	    #:title title))
	 #:post-template post-template))


;; index page

(define (post-uri site post)
  (string-append "/news/" (site-post-slug site post) ".html"))

(define hero-nav
  `(section (@ (class "hero-nav hero is-success is-medium"))
	    (div (@ (class "hero-head"))
		 (nav (@ (class "navbar is-fixed"))
		      (div (@ (class "container"))
			   (div (@ (class "navbar-brand"))
				(a (@ (class "navbar-item")
				      (href  "https://breaktheborders.gr"))
				   (img (@ (src "/assets/img/btb.jpg")
					   (width "56") (height "28"))))
				(span (@ (class "navbar-burger burger")
					 (data-target "navbarMenuHeroA"))
				      (span)
				      (span)
				      (span)))
			   (div (@ (id "navbarMenuHeroA")
				   (class "navbar-menu"))
				(div (@ (class "navbar-end"))
				     (a (@ (class "navbar-item"))
					"Home")
				     (a (@ (class "navbar-item"))
					"About")
				     (a (@ (class "navbar-item"))
					"News")
				     (a (@ (class "navbar-item"))
					"Contact")))))
		 (div (@ (class "hero-body"))
		      (div (@ (class "container"))
			   (h1 (@ (class "title logo-title"))
			       "Break the Borders")
			   (h2 (@ (class "subtitle"))
			       "Always Learning"))))))

(define about
  `(section (@ (class "about"))
	    (div (@ (class "container"))
		 (h1 (@ (class "title"))
		     "Who we are")
		 (p (@ (class "is-size-5"))
		    "Break the Borders is a non-profit organization
that works on youth initiatives development, realization of innovative
projects and unites active citizens of Greece on a voluntary basis.")
		 (br)
		 (p (@ (class "is-size-5"))
		    "The organization's aim is purely educational,
cultural and non-profit, and consists in taking initiatives and
practical action in the fields of culture, social responsibility,
solidarity and ecology with particular emphasis on the contribution to
personal development as citizen of the world, through active
citizenship in Greece, Europe and worldwide as well.")
		 (br)
		 (p (@ (class "is-size-5"))
		    "Break the Borders is the successor of the
dissolved Breaking the Borders association, active on local level in
different fields, as cooperating with “Lighthouse for the blind” in
Athens and Epanodos for a 6 month project about art as educational
tool in prison. On international level, Breaking the borders was one
of the founding members of Art In (E)motion, and has organised and
facilitated one youth exchange and the one of the five learning events
of “Art in (E)motion, New A.I.M.’s for non-formal learning”, Grundtvig
project.  Continuity is provided by the presence in Break the Borders,
of most of the staff responsible for the organization and
implementation of projects already active in the previous
organization. Currently, Break the Borders is participating in various
european projects as a partner organisation and will coordinate
“V-isability”, an EVS project in collaboration with the Center for
Education and Rehabilitation for the Blind."))))

(define hero-values
  `(section (@ (class "hero-values hero is-warning is-medium has-text-centered"))
	    (div (@ (class "hero-body"))
		 (div (@ (class "container"))
		      (div (@ (class "columns"))
		       (div (@ (class "column is-one-quarter"))
			    (div (@ (class "icon is-large has-text-success"))
				 (i (@ (class "far fa-3x fa-heart"))))
			    (div (@ (class "title"))
				 "Human Rights"))
		       (div (@ (class "column is-one-quarter"))
			    (div (@ (class "icon is-large has-text-success"))
				 (i (@ (class "fas fa-3x fa-fist-raised"))))
			    (div (@ (class "title"))
				 "Active Citizenship"))
		       (div (@ (class "column is-one-quarter"))
			    (div (@ (class "icon is-large has-text-success"))
				 (i (@ (class "fas fa-3x fa-leaf"))))
			    (div (@ (class "title"))
				 "Ecology"))
		       (div (@ (class "column is-one-quarter"))
			    (div (@ (class "icon is-large has-text-success"))
				 (i (@ (class "fas fa-3x fa-hands-helping"))))
			    (div (@ (class "title"))
				 "Solidarity")))))))

(define (news-feed site posts)
  `(section (@ (class "news-feed"))
	    (div (@ (class "container"))
		 (h1 (@ (class "title")) "Latest News")
		 (div ,(map (lambda (post)
			      `(article (@ (class "message is-warning is-light"))
					(div (@ (class "message-body"))
					     (h1 (a (@ (class "is-size-5")
						     (href ,(post-uri site post)))
						 ,(post-ref post 'title)))
					     (h2 ,(date->string* (post-date post))))))
			    (take-up-to 10 (posts/reverse-chronological posts)))))))

(define (index-content site posts)
  `(div ,hero-nav ,about ,hero-values ,(news-feed site posts)))

(define (index-page site posts)
  (make-page "index.html"
	     (base-tmpl site
			(index-content site posts))
	     sxml->html))

(site #:title "Break the Borders"
      #:domain "breaktheborders.gr"
      #:readers (list skribe-reader html-reader)
      #:builders (list (blog #:prefix "/news"
			     #:theme btb-haunt-theme)
		       index-page
		       (static-directory "assets" "assets")
		       (atom-feed #:blog-prefix "/news")))
