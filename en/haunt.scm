(use-modules (haunt asset)
	     (haunt site)
	     (haunt page)
	     (haunt post)
	     (haunt html)
	     (haunt utils)
	     (haunt builder blog)
	     (haunt builder atom)
	     (haunt builder assets)
	     (haunt reader)
	     (haunt reader skribe)
	     (jakob reader html-prime)
             (gallery gallery))
;; theme

(define contact
  `(section (@ (class "hero-contact hero contact is-success")
	       (id "contact"))
	    (div (@ (class "container"))
		 (h1 (@ (class "title"))
		     "Never hesitate to contact us!")
		 (form (@ (action "https://formspree.io/xrgbqglw")
			  (method "POST")
			  (id "contact-form"))
		  (div (@ (class "field"))
		       (label (@ (class "laber")) "From:")
		       (div (@ (class "field-body"))
			    (div (@ (class "field"))
				 (p (@ (class "control is-expanded has-icons-left"))
				    (input (@ (class "input")
					      (type "text")
					      (name "name")
                                              (required "")
					      (placeholder "Name")))
				    (span (@ (class "icon is-small is-left"))
					  (i (@ (class "fas fa-user")))))))
		       (div (@ (class "field-body"))
			    (div (@ (class "field"))
				 (p (@ (class "control is-expanded has-icons-left"))
				    (input (@ (class "input")
					      (type "email")
					      (name "_replyto")
					      (placeholder "E-mail")))
				    (span (@ (class "icon is-small is-left"))
					  (i (@ (class "fas fa-envelope"))))))))
		  (div (@ (class "field"))
		       (label (@ (class "laber")) "Subject:")
		       (div (@ (class "field-body"))
			    (div (@ (class "field"))
				 (div (@ (class "control"))
				      (input (@ (class "input")
						(type "text")
                                                (required "")
						(placeholder "Subject")))))))
		  (div (@ (class "field"))
		       (label (@ (class "laber")) "Message:")
		       (div (@ (class "field-body"))
			    (div (@ (class "field"))
				 (div (@ (class "control"))
				      (textarea (@ (class "textarea")
						   (name "message")
                                                   (required "")
						   (placeholder "Your message here!"))))))))
		 (div (@ (class "field"))
		      (div (@ (class "field")))
		      (div (@ (class "field-body"))
			   (button (@ (class "button is-white")
				      (type "submit")
				      (form "contact-form")
				      (value "Submit"))
				   "Send message"))))))
(define footer
  `(footer (@ (class "footer"))
	   (div (@ (class "columns"))
		(div (@ (class "column"))
		     (h1 "Connect with us")
		     (a (@ (class "has-text-white")
			   (href "https://www.facebook.com/BreakTheBordersNGO/"))
			(span (@ (class "icon"))
			      (i (@ (class "fab fa-facebook-f")))))
		     (a (@ (class "has-text-white")
			   (href "mailto:breaktheborders@yahoo.com"))
			(span (@ (class "icon is-large has-text-white"))
			      (i (@ (class "fas fa-envelope"))))))
		(div (@ (class "column"))
		     (p (@ (class "footer-middle"))
			(span (@ (class "icon is-large"))
			      (i (@ (class "far fa-copyright"))))
			"Break the Borders, 2021")
		     (p "All content is shared under the
Creative Commons Attribution-Sharealike 4.0 International license."))
		(div (@ (class "column footer-right"))
		     (p "Designed by "
			(a (@ (href "https://bendersteed.gitlab.io"))
			   "bendersteed") " with lot's of love.")
		     (p "Powered by "
			(a (@ (href "http://haunt.dthompson.us/"))
			   "Haunt") ".")))))


(define* (base-tmpl site body #:key title)
  `((doctype html)
    (head
     (meta (@ (charset "utf-8")))
     (meta (@ (name "viewport")
	      (content "width=device-width, initial-scale=1")))
     (title ,(if title
		 (string-append title " | Break the Borders")
		 (site-title site)))
     ;; css
     (link (@ (rel "stylesheet")
	      (href "https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css")))
     
     (link (@ (rel "stylesheet")
	      (href "https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap")))
     (link (@ (rel "stylesheet")
	      (href "/assets/btb.css")))
     ;; atom feed
     (link (@ (rel "alternate")
	      (title "ActivityPub news")
	      (type "application/atom+xml")
	      (href "/feed.xml")))
     (script (@ (defer "")
		(src "https://kit.fontawesome.com/aa442d8292.js")
		(crossorigin "anonymous")))
     (script (@ (src "/assets/js/in-view.js")))
     
     (body (@ (class "has-navbar-fixed-top"))
	   ,body
	   ,footer)
     (script (@ (src "/assets/js/scrollspy.js")))
     (script (@ (src "/assets/js/smooth-scrolling.js")))
     (script (@ (src "/assets/js/init.js"))))))

(define (hero-nav-post post)
  `(section (@ (class "hero-nav hero is-success is-small"))
	    (div (@ (class "hero-head"))
		 (div (@ (class "hero-body"))
		      (div (@ (class "container"))
			   (h1 (@ (class "title"))
			       ,(post-ref post 'title))
			   (h2 (@ (class "subtitle"))
			       ,(date->string* (post-date post))))))))

(define (post-template post)
  `(,(nav post) ,(hero-nav-post post)
	(section (@ (class "article-content"))
		 (div (@ (class "container is-size-5"))
		      ,(post-sxml post)))))

(define (hero-nav-news)
  `(section (@ (class "hero-nav hero is-success is-small"))
	    (div (@ (class "hero-head"))
		 (div (@ (class "hero-body"))
		      (div (@ (class "container"))
			   (h1 (@ (class "title"))
			       "Archive of our news"))))))

(define (collection-template site title posts prefix)
  `(,(nav posts) ,(hero-nav-news)
	(section (@ (class "article-content"))
		 (div (@ (class "container"))
		  (div (@ (class "columns news-listing"))
		   ,(create-news-feed site posts))))))

(define btb-haunt-theme
  (theme #:name "Break the Borders"
	 #:layout
	 (lambda (site title body)
	   (base-tmpl
	    site body
	    #:title title))
	 #:post-template post-template
	 #:collection-template collection-template))


;; index page

(define (post-uri site post)
  (string-append "/en/news/" (site-post-slug site post) ".html"))

(define* (nav #:optional post)
  `(nav (@ (class "navbar is-fixed-top"))
	(div (@ (class "container"))
	     (div (@ (class "navbar-brand"))
		  (a (@ (class "navbar-item")
			(href  "https://breaktheborders.gr"))
		     (img (@ (src "/assets/img/btb.jpg")
			     (width "56") (height "28"))))
		  (span (@ (class "navbar-burger burger")
			   (data-target "navbarMenuHeroA"))
			(span)
			(span)
			(span)))
	     (div (@ (id "navbarMenuHeroA")
		     (class "navbar-menu"))
		  (div (@ (class "navbar-end"))
		       (a (@ (class "navbar-item")
			     (href "https://www.facebook.com/BreakTheBordersNGO/"))
			  (span (@ (class "icon"))
				(i (@ (class "fab fa-facebook-f")))))
		       (a (@ (class "navbar-item")
			     (href "mailto:breaktheborders@yahoo.com"))
			  (span (@ (class "icon"))
				(i (@ (class "fas fa-envelope")))))
		       (a (@ (class "navbar-item")
			     (id "link-about")
			     (href ,(if post
					"/en#about"
					"#about")))
			  "About")
		       (a (@ (class "navbar-item")
			     (id "link-news")
			     (href ,(if post
					"/en#news"
					"#news")))
			  "News")
		       (a (@ (class "navbar-item")
			     (id "link-contact")
			     (href ,(if post
					"/en#contact"
					"#contact")))
			  "Contact")
                       (a (@ (class ,(if (and (string? post)
                                              (string= post "activities.html"))
                                         "active navbar-item"
                                         "navbar-item"))
			     (id "link-activities")
			     (href "/en/activities.html"))
			  "Activities")
		       (div (@ (class "navbar-item lang-picker has-dropdown is-hoverable"))
			    (a (@ (class "navbar-link"))
			       "EN")
			    (div (@ (class "navbar-dropdown"))
				 (p (@ (class "navbar-item"))
				    "Choose language:")
				 (hr (@ (class "navbar-divider")))
				 (a (@ (class "navbar-item")
				       (href ,(cond
                                               ((string? post) (string-append "/en/" post))
					       ((list? post) "/en/news")
					       (post (string-append "/en/news/" (post-slug post) ".html"))
					       (else "/en"))))
				    "English")
				 (a (@ (class "navbar-item")
				       (href ,(cond
                                               ((string? post) (string-append "/el/" post))
					       ((list? post) "/el/news")
					       (post (string-append "/el/news/" (post-slug post) ".html"))
					       (else "/el"))))
				    "Ελληνικά"))))))))

(define hero-nav
  `(section (@ (class "hero-nav hero is-medium is-success"))
	    (div (@ (class "hero-head"))
		 (div (@ (class "hero-body"))
		      (div (@ (class "container"))
			   (h1 (@ (class "title logo-title"))
			       "Break the Borders")
			   (h2 (@ (class "subtitle"))
			       "Always Learning"))))))

(define about
  `(section (@ (id "about"))
	    (div (@ (class "container"))
		 (h1 (@ (class "title"))
		     "Who we are")
		 (p (@ (class "is-size-5"))
		    "Break the Borders is a non-profit organization
that works on youth initiatives development, realization of innovative
projects and unites active citizens of Greece on a voluntary basis.")
		 (br)
		 (p (@ (class "is-size-5"))
		    "The organization's aim is purely educational,
cultural and non-profit, and consists in taking initiatives and
practical action in the fields of culture, social responsibility,
solidarity and ecology with particular emphasis on the contribution to
personal development as citizen of the world, through active
citizenship in Greece, Europe and worldwide as well.")
		 (br)
		 (p (@ (class "is-size-5"))
		    "Break the Borders is the successor of the
dissolved Breaking the Borders association, active on local level in
different fields, as cooperating with “Lighthouse for the blind” in
Athens and Epanodos for a 6 month project about art as educational
tool in prison. On international level, Breaking the borders was one
of the founding members of Art In (E)motion, and has organised and
facilitated one youth exchange and the one of the five learning events
of “Art in (E)motion, New A.I.M.’s for non-formal learning”, Grundtvig
project.  Continuity is provided by the presence in Break the Borders,
of most of the staff responsible for the organization and
implementation of projects already active in the previous
organization. Currently, Break the Borders is participating in various
european projects as a partner organisation and will coordinate
“V-isability”, an EVS project in collaboration with the Center for
Education and Rehabilitation for the Blind."))))

(define hero-values
  `(section (@ (class "hero-values hero is-warning is-medium has-text-centered"))
	    (div (@ (class "hero-body"))
		 (div (@ (class "container"))
		      (div (@ (class "columns"))
		       (div (@ (class "column is-one-quarter"))
			    (div (@ (class "icon is-large has-text-success"))
				 (i (@ (class "far fa-3x fa-heart"))))
			    (div (@ (class "title"))
				 "Human Rights"))
		       (div (@ (class "column is-one-quarter"))
			    (div (@ (class "icon is-large has-text-success"))
				 (i (@ (class "fas fa-3x fa-fist-raised"))))
			    (div (@ (class "title"))
				 "Active Citizenship"))
		       (div (@ (class "column is-one-quarter"))
			    (div (@ (class "icon is-large has-text-success"))
				 (i (@ (class "fas fa-3x fa-leaf"))))
			    (div (@ (class "title"))
				 "Ecology"))
		       (div (@ (class "column is-one-quarter"))
			    (div (@ (class "icon is-large has-text-success"))
				 (i (@ (class "fas fa-3x fa-hands-helping"))))
			    (div (@ (class "title"))
				 "Solidarity")))))))

(define* (news-feed site posts #:optional (n #f))
  `(section (@ (id "news"))
	    (div (@ (class "container"))
		 (h1 (@ (class "title")) "Latest News "
		     (a (@ (href "/en/feed.xml")) (span (@ (class "icon"))
				                        (i (@ (class "fas fa-rss"))))))
		 (div (@ (class "columns news-listing"))
		      ,(create-news-feed site posts n))
		 (div (@ (class "columns is-centered")
			 (id "all-news"))
		      (div (@ (class "button"))
			   (a (@ (href "/en/news"))
			      "Read all news"))))))

(define* (create-news-feed site posts #:optional (n #f))
  (map (lambda (post)
	 `(div (@ (class "column card is-one-quarter"))
	       (div (@ (class "card-image"))
		    (figure (@ (class "image is-4by3"))
			    (img (@ (src ,(post-ref post 'image))))))
	       ((div (@ (class "card-content"))
		     (h1 (a (@ (class "is-size-5")
			       (href ,(post-uri site post)))
			    ,(post-ref post 'title)))
		     (h2 ,(date->string* (post-date post)))))))
       (if n
	   (take-up-to n (posts/reverse-chronological posts))
	   (posts/reverse-chronological posts))))

(define (index-content site posts)
  `(,(nav) ,hero-nav ,about ,hero-values ,(news-feed site posts 5) ,contact))

(define (index-page site posts)
  (make-page "index.html"
	     (base-tmpl site
			(index-content site posts))
	     sxml->html))

;;; activities page
(define* (hero-nav-activities #:optional title subtitle) 
  `(section (@ (class "hero-nav hero is-small is-success"))
	    (div (@ (class "hero-head"))
		 (div (@ (class "hero-body"))
		      (div (@ (class "container"))
			   (h1 (@ (class "title logo-title"))
			       ,(if title
                                    title
                                    "Activities"))
			  (h2 (@ (class "subtitle"))
			      ,(if subtitle
                                   subtitle
                                   "Tools developed for a digital age")))))))

(define activities-feed
  `(section (@ (class "article-content"))
	    (div (@ (class "container"))
		 (div (@ (class "columns news-listing"))
		      (div (@ (class "column card is-one-third"))
	                   (div (@ (class "card-image"))
		                (figure (@ (class "image is-4by3"))
			                (img (@ (src "/assets/img/step.jpg")
                                                (alt "image of one step forward")))))
	                   ((div (@ (class "card-content"))
		                 (h1 (a (@ (class "is-size-5")
			                   (href "one-step-forward.html"))
			                "One step forward")))))
                      (div (@ (class "column card is-one-third"))
	                   (div (@ (class "card-image"))
		                (figure (@ (class "image is-4by3"))
			                (img (@ (src "/assets/img/i-have.jpg")
                                                (alt "image of one step forward")))))
	                   ((div (@ (class "card-content"))
		                 (h1 (a (@ (class "is-size-5")
			                   (href "i-have.html"))
			                "I have/I haven't")))))
                      (div (@ (class "column card is-one-third"))
	                   (div (@ (class "card-image"))
		                (figure (@ (class "image is-4by3"))
			                (img (@ (src "/assets/img/gallery.jpg")
                                                (alt "image of one step forward")))))
	                   ((div (@ (class "card-content"))
		                 (h1 (a (@ (class "is-size-5")
			                   (href "gallery.html"))
			                "Gallery")))))))))

(define (activities-content site posts)
  `(,(nav "activities.html") ,(hero-nav-activities) ,activities-feed))

(define (activities-page site posts)
  (make-page "activities.html"
             (base-tmpl site
                        (activities-content site posts))
             sxml->html))

;;; one step forward
(define one-step-activity
  `(section (@ (class "article-content")
               (id "activity"))
            (script (@ (src "https://pixijs.download/release/pixi.min.js")))
            (script (@ (src "/assets/js/one-step-forward.js")))
            (div (@ (class "container")
                    (id "main"))
                 (div (@ (class "columns")
                         (id "config"))
                      (div (@ (class "column description"))
                           (p "This is a digital companion for implementing the One step forward activity through
video conference. Choose the number of participants and their designated roles and then share the
screen so the participants have a visual approach to the results of the activity. At the end of the
questions you can choose to show the hidden roles."))
                      (div (@ (class "column"))
                           (article (@ (class "message is-danger")
                                       (id "warning")
                                       (style "display: none;"))
                                    (div (@ (class "message-header"))
                                         (p "Warning")
                                         (button (@ (class "delete")
                                                    (aria-label "delete")
                                                    (onclick "closeWarning()"))))
                                    (div (@ (class "mesage-body"))
                                         "The number of participants needs to be the same as that of the roles!"))
                           (form (@ (action "test")
                                    (id "one-step-form"))
		                 (div (@ (class "field"))
		                      (label (@ (class "label")) "Number of participants:")
		                      (div (@ (class "field-body"))
			                   (div (@ (class "field"))
				                (p (@ (class "control"))
				                   (input (@ (class "input")
                                                             (id "number-of-participants")
					                     (type "number")
					                     (name "no_part")))))))
                                 (div (@ (class "field"))
		                      (label (@ (class "label")) "Roles (comma separated):")
		                      (div (@ (class "field-body"))
			                   (div (@ (class "field"))
				                (p (@ (class "control"))
				                   (input (@ (class "input")
                                                             (id "number-of-participants")
					                     (type "text")
					                     (name "roles")))))))
		                 (div (@ (class field))
                                      (label (@ (class "label")) "Number of questions:")
                                      (div (@ (class "field-body"))
			                   (div (@ (class "field"))
				                (p (@ (class "control"))
				                   (input (@ (class "input")
					                     (type "number")
					                     (name "no_questions"))))))))
		           (div (@ (class "field"))
		                (div (@ (class "field-body"))
			             (button (@ (class "button is-white")
                                                (id "one-step-start")
                                                (onclick "setup()"))
				             "Start!"))))))))

(define (one-step-content site posts)
  `(,(nav "") ,(hero-nav-activities "One step forward") ,one-step-activity))

(define (one-step-page site posts)
  (make-page "one-step-forward.html"
             (base-tmpl site
                        (one-step-content site posts))
             sxml->html))

;;; i have/i haven't
(define i-have-activity
  `(section (@ (class "article-content")
               (id "activity"))
            (script (@ (src "https://pixijs.download/release/pixi.min.js")))
            (script (@ (src "/assets/js/i-have.js")))
            (div (@ (class "container")
                    (id "main"))
                 (div (@ (class "columns")
                         (id "config"))
                      (div (@ (class "column description"))
                           (p "This is a digital companion for implementing the I have/I haven't
activity through video conference. Choose the number of participants and then share the screen so
the participants have a visual approach to the results of the activity."))
                      (div (@ (class "column"))
                           (form (@ (action "test")
                                    (id "i-have-form"))
		                 (div (@ (class "field"))
		                      (label (@ (class "label")) "Number of participants:")
		                      (div (@ (class "field-body"))
			                   (div (@ (class "field"))
				                (p (@ (class "control"))
				                   (input (@ (class "input")
                                                             (id "number-of-participants")
					                     (type "number")
					                     (name "no_part"))))))))
		           (div (@ (class "field"))
		                (div (@ (class "field-body"))
			             (button (@ (class "button is-white")
                                                (id "i-have-start")
                                                (onclick "setup()"))
				             "Start!"))))))))

(define (i-have-content site posts)
  `(,(nav "") ,(hero-nav-activities "I have/I haven't") ,i-have-activity))

(define (i-have-page site posts)
  (make-page "i-have.html"
             (base-tmpl site
                        (i-have-content site posts))
             sxml->html))

;;; gallery activity
(define (gallery-activity)
  `(section (@ (class "article-content")
               (id "activity"))
            (div (@ (class "container")
                    (id "main"))
                 (div (@ (class "columns")
                         (id "config"))
                      (div (@ (class "column description"))
                           (p "A digital gallery with support for comments. Trying to recreate the
feeling of being in a real world gallery but influence from social media is inevitable."))
                      (div (@ (class "column"))
		           (div (@ (class "field"))
		                (div (@ (class "field-body"))
			             (a (@ (href "/en/gallery1.html"))
                                        (button (@ (class "button is-white"))
				                "Get in the Gallery")))))))))

(define (gallery-content site posts)
  `(,(nav "") ,(hero-nav-activities "Gallery") ,(gallery-activity)))

(define (gallery-page site posts)
  (make-page "gallery.html" 
             (base-tmpl site
                        (gallery-content site posts))
             sxml->html))

(site #:title "Break the Borders"
      #:domain "breaktheborders.gr"
      #:readers (list skribe-reader html-reader-prime)
      #:builders (list (blog #:prefix "/news"
			     #:theme btb-haunt-theme)
		       index-page
                       activities-page
                       one-step-page
                       i-have-page
                       gallery-page
                       (gallery)
		       (static-directory "assets" "assets")
		       (atom-feed #:blog-prefix "/news")))
