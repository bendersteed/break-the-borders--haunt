function grabFormData() {
    let form = new FormData(document.getElementById("i-have-form"));
    const key = "no_part";

    return parseInt(form.get(key));
}

let texture;
let app;
let space_global;

function setup() {
    const no_part = grabFormData();
        
    config = document.getElementById("config");
    config.style = "display: none;";

    main = document.getElementById("main");
    app = new PIXI.Application({ backgroundColor: 0xffffff,
                                 width: 1000,
                                 height: 700});
    main.appendChild(app.view);

    texture = PIXI.Texture.from('../assets/js/bunny.png');
    texture.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
    const texture_height = 26;
    const texture_width = 37;

    const space_avail = app.screen.height / no_part;
    space_global = space_avail;
    const scale = calcScale(space_avail, texture_height);
    const margin = space_avail - (scale * texture_height);

    drawGridLine();
    
    for (let i = 0; i < no_part; i++) {
        createBunny(
            Math.floor(app.screen.width / 2),
            Math.floor(i * space_avail + 0.7 * scale * texture_height),
            scale
        );
    }
}

function createBunny(x, y, scale) {
    const bunny = new PIXI.Sprite(texture);
    bunny.interactive = true;
    bunny.buttonMode = true;
    bunny.anchor.set(0.5);
    bunny.scale.set(scale);
    bunny.tint = Math.random() * 0xFFFFFF;

    bunny
        .on('pointerdown', onDragStart)
        .on('pointerup', onDragEnd)
        .on('pointerupoutside', onDragEnd)
        .on('pointermove', onDragMove);

    bunny.x = x;
    bunny.y = y;
    app.stage.addChild(bunny);
}

function onDragStart(event) {
    this.data = event.data;
    this.alpha = 0.5;
    this.dragging = true;
}

function onDragEnd() {
    this.alpha = 1;
    this.dragging = false;
    this.data = null;
}

function onDragMove() {
    if (this.dragging) {
        const newPosition = this.data.getLocalPosition(this.parent);
        this.x = newPosition.x;
        this.y = newPosition.y;
    }
}

function calcScale(space_avail, texture_size) {
    if (texture_size >= space_avail) {
        return 0;
    } else {
        return 1 + calcScale(space_avail, 2 * texture_size);
    }
}

function drawGridLine() {
    const x = app.screen.width / 2;
    let line = new PIXI.Graphics();
    line.lineStyle(2,0x48c774, 0.85);
    line.moveTo(x, 0);
    line.lineTo(x, app.screen.height);
    app.stage.addChild(line);
}
