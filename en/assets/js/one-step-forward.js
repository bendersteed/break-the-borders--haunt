function grabFormData() {
    let form = new FormData(document.getElementById("one-step-form"));
    const names = ["no_part", "roles", "no_questions"];

    return {
        no_part: parseInt(form.get(names[0])),
        roles: form.get(names[1]).split(","),
        no_questions: parseInt(form.get(names[2]))
    };
}

function checkFormData(obj) {
    const {no_part, roles, no_questions} = obj;
    return !!no_questions && no_part == roles.length;
}

function closeWarning() {
    document.getElementById("warning").style = "display: none;"
}

let texture;
let app;
let roles_global;
let space_global;

function setup() {
    const obj = grabFormData();
    
    if (checkFormData(obj)) {
        const {no_part, roles, no_questions} = obj;
        roles_global = roles;
        config = document.getElementById("config");
        config.style = "display: none;";

        main = document.getElementById("main");
        app = new PIXI.Application({ backgroundColor: 0xffffff,
                                     width: 1000,
                                     height: 700});
        main.appendChild(app.view);

        texture = PIXI.Texture.from('../assets/js/bunny.png');
        texture.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
        const texture_height = 26;
        const texture_width = 37;

        const space_avail = app.screen.height / no_part;
        space_global = space_avail;
        const scale = calcScale(space_avail, texture_height);
        const margin = space_avail - (scale * texture_height);
        
        for (let i = 0; i < no_part; i++) {
            createBunny(
                Math.floor(texture_width),
                Math.floor(i * space_avail + 0.7 * scale * texture_height),
                scale
            );
        }

        question_space = app.screen.width / (no_questions + 1);
        for (let i = 1; i < (no_questions + 1); i++) {
            drawGridLine(i * question_space);
        }

        revealButton();
    } else {
        const warning = document.getElementById("warning");
        warning.style = "display: block;";
    }
}

function revealButton() {
    const bunny = new PIXI.Sprite(texture);
    bunny.interactive = true;
    bunny.buttonMode = true;
    bunny.scale.set(1);
    bunny.tint = Math.random() * 0xFFFFFF;

    bunny
        .on('pointerdown', onPress)
        .on('pointerup', onRelease)

    bunny.x = 700;
    bunny.y = 10;
    app.stage.addChild(bunny);
}

function onPress() {
    this.alpha = 0.5;
}

function onRelease() {
    this.alpha = 1;
    for (let i = 0; i < roles_global.length; i++) {
        drawRoleBox(roles_global[i], 10, space_global * (0.1 + i));
    }
}

function createBunny(x, y, scale) {
    const bunny = new PIXI.Sprite(texture);
    bunny.interactive = true;
    bunny.buttonMode = true;
    bunny.anchor.set(0.5);
    bunny.scale.set(scale);
    bunny.tint = Math.random() * 0xFFFFFF;

    bunny
        .on('pointerdown', onDragStart)
        .on('pointerup', onDragEnd)
        .on('pointerupoutside', onDragEnd)
        .on('pointermove', onDragMove);

    bunny.x = x;
    bunny.y = y;
    app.stage.addChild(bunny);
}

function onDragStart(event) {
    this.data = event.data;
    this.alpha = 0.5;
    this.dragging = true;
}

function onDragEnd() {
    this.alpha = 1;
    this.dragging = false;
    this.data = null;
}

function onDragMove() {
    if (this.dragging) {
        const newPosition = this.data.getLocalPosition(this.parent);
        this.x = newPosition.x;
        this.y = newPosition.y;
    }
}

function calcScale(space_avail, texture_size) {
    if (texture_size >= space_avail) {
        return 0;
    } else {
        return 1 + calcScale(space_avail, 2 * texture_size);
    }
}

function drawGridLine(x) {
    let line = new PIXI.Graphics();
    line.lineStyle(2,0x48c774, 0.85);
    line.moveTo(x,0);
    line.lineTo(x,app.stage.height);
    app.stage.addChild(line);
}

function drawRoleBox(text, x, y) {
    let message = new PIXI.Text(text,
                                { wordWrap: false });
    message.x = x;
    message.y = y;

    app.stage.addChild(message);
}
