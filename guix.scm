(use-modules (guix packages)
             (guix licenses)
             (guix build-system gnu)
	     (gnu packages guile-xyz))

(package
  (name "break-the-borders")
  (version "0.0.1")
  (source #f) ; not needed just to create dev environment
  (build-system gnu-build-system)
  ;; These correspond roughly to "development" dependencies.
  (propagated-inputs
   `("haunt" ,haunt))
  (synopsis "A static website for the Break the Borders NGO")
  (description "A website promoting the goals of the organisation and
offering contact for intersted people.")
  (home-page "https://breaktheborders.gr")
  (license gpl3+))
