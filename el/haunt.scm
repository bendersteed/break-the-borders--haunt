(use-modules (haunt asset)
	     (haunt site)
	     (haunt page)
	     (haunt post)
	     (haunt html)
	     (haunt utils)
	     (haunt builder blog)
	     (haunt builder atom)
	     (haunt builder assets)
	     (haunt reader)
	     (haunt reader skribe)
	     (jakob reader html-prime))
;; theme

(setlocale LC_ALL "el_GR.utf8")

(define contact
  `(section (@ (class "hero-contact hero contact is-success")
	       (id "contact"))
	    (div (@ (class "container"))
		 (h1 (@ (class "title"))
		     "Επικοινωνείστε μαζί μας!")
		 (form (@ (action "https://formspree.io/xrgbqglw")
			  (method "POST")
			  (id "contact-form"))
		  (div (@ (class "field"))
		       (label (@ (class "laber")) "Από:")
		       (div (@ (class "field-body"))
			    (div (@ (class "field"))
				 (p (@ (class "control is-expanded has-icons-left"))
				    (input (@ (class "input")
					      (type "text")
					      (name "name")
					      (placeholder "Όνομα")))
				    (span (@ (class "icon is-small is-left"))
					  (i (@ (class "fas fa-user")))))))
		       (div (@ (class "field-body"))
			    (div (@ (class "field"))
				 (p (@ (class "control is-expanded has-icons-left"))
				    (input (@ (class "input")
					      (type "email")
					      (name "_replyto")
					      (placeholder "E-mail")))
				    (span (@ (class "icon is-small is-left"))
					  (i (@ (class "fas fa-envelope"))))))))
		  (div (@ (class "field"))
		       (label (@ (class "laber")) "Θέμα:")
		       (div (@ (class "field-body"))
			    (div (@ (class "field"))
				 (div (@ (class "control"))
				      (input (@ (class "input")
						(type "text")
						(placeholder "Θέμα")))))))
		  (div (@ (class "field"))
		       (label (@ (class "laber")) "Μήνυμα:")
		       (div (@ (class "field-body"))
			    (div (@ (class "field"))
				 (div (@ (class "control"))
				      (textarea (@ (class "textarea")
						   (name "message")
						   (placeholder "Γράψτε το μήνυμα σας εδώ!"))))))))
		 (div (@ (class "field"))
		      (div (@ (class "field")))
		      (div (@ (class "field-body"))
			   (button (@ (class "button is-white")
				      (type "submit")
				      (form "contact-form")
				      (value "Submit"))
				   "Αποστολή μηνύματος"))))))
(define footer
  `(footer (@ (class "footer"))
	   (div (@ (class "columns"))
		(div (@ (class "column"))
		     (h1 "Connect with us")
		     (a (@ (class "has-text-white")
			   (href "https://www.facebook.com/BreakTheBordersNGO/"))
			(span (@ (class "icon"))
			      (i (@ (class "fab fa-facebook-f")))))
		     (a (@ (class "has-text-white")
			   (href "mailto:breaktheborders@yahoo.com"))
			(span (@ (class "icon is-large has-text-white"))
			      (i (@ (class "fas fa-envelope"))))))
		(div (@ (class "column"))
		     (p (@ (class "footer-middle"))
			(span (@ (class "icon is-large"))
			      (i (@ (class "far fa-copyright"))))
			"Break the Borders, 2021")
		     (p "All content is shared under the
Creative Commons Attribution-Sharealike 4.0 International license."))
		(div (@ (class "column footer-right"))
		     (p "Designed by "
			(a (@ (href "https://bendersteed.gitlab.io"))
			   "bendersteed") " with lot's of love.")
		     (p "Powered by "
			(a (@ (href "http://haunt.dthompson.us/"))
			   "Haunt") ".")))))


(define* (base-tmpl site body #:key title)
  `((doctype html)
    (head
     (meta (@ (charset "utf-8")))
     (meta (@ (name "viewport")
	      (content "width=device-width, initial-scale=1")))
     (title ,(if title
		 (string-append title " | Break the Borders")
		 (site-title site)))
     ;; css
     (link (@ (rel "stylesheet")
	      (href "https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css")))
     
     (link (@ (rel "stylesheet")
	      (href "https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap")))
     (link (@ (rel "stylesheet")
	      (href "/assets/btb.css")))
     ;; atom feed
     (link (@ (rel "alternate")
	      (title "ActivityPub news")
	      (type "application/atom+xml")
	      (href "/feed.xml")))
     (script (@ (defer "")
		(src "https://kit.fontawesome.com/aa442d8292.js")
		(crossorigin "anonymous")))
     (script (@ (src "/assets/js/in-view.js")))
     
     (body (@ (class "has-navbar-fixed-top"))
	   ,body
	   ,footer)
     (script (@ (src "/assets/js/scrollspy.js")))
     (script (@ (src "/assets/js/smooth-scrolling.js")))
     (script (@ (src "/assets/js/init.js"))))))

(define (hero-nav-post post)
  `(section (@ (class "hero-nav hero is-success is-small"))
	    (div (@ (class "hero-head"))
		 (div (@ (class "hero-body"))
		      (div (@ (class "container"))
			   (h1 (@ (class "title"))
			       ,(post-ref post 'title))
			   (h2 (@ (class "subtitle"))
			       ,(date->string* (post-date post))))))))

(define (post-template post)
  `(,(nav post) ,(hero-nav-post post)
	(section (@ (class "article-content"))
		 (div (@ (class "container is-size-5"))
		      ,(post-sxml post)))))

(define (hero-nav-news)
  `(section (@ (class "hero-nav hero is-success is-small"))
	    (div (@ (class "hero-head"))
		 (div (@ (class "hero-body"))
		      (div (@ (class "container"))
			   (h1 (@ (class "title"))
			       "Archive of our news"))))))

(define (collection-template site title posts prefix)
  `(,(nav posts) ,(hero-nav-news)
	(section (@ (class "article-content"))
		 (div (@ (class "container"))
		  (div (@ (class "columns news-listing"))
		   ,(create-news-feed site posts))))))

(define btb-haunt-theme
  (theme #:name "Break the Borders"
	 #:layout
	 (lambda (site title body)
	   (base-tmpl
	    site body
	    #:title title))
	 #:post-template post-template
	 #:collection-template collection-template))


;; index page

(define (el-post-slug post)
  "Since el posts need to have the same slug as en ones we generate
the slug for these posts not from title but from an additional slug
metadata."
  (post-ref post 'slug))

(define (post-uri site post)
  (string-append "/el/news/" (site-post-slug site post) ".html"))

(define* (nav #:optional post)
  `(nav (@ (class "navbar is-fixed-top"))
	(div (@ (class "container"))
	     (div (@ (class "navbar-brand"))
		  (a (@ (class "navbar-item")
			(href  "https://breaktheborders.gr"))
		     (img (@ (src "/assets/img/btb.jpg")
			     (width "56") (height "28"))))
		  (span (@ (class "navbar-burger burger")
			   (data-target "navbarMenuHeroA"))
			(span)
			(span)
			(span)))
	     (div (@ (id "navbarMenuHeroA")
		     (class "navbar-menu"))
		  (div (@ (class "navbar-end"))
		       (a (@ (class "navbar-item")
			     (href "https://www.facebook.com/BreakTheBordersNGO/"))
			  (span (@ (class "icon"))
				(i (@ (class "fab fa-facebook-f")))))
		       (a (@ (class "navbar-item")
			     (href "mailto:breaktheborders@yahoo.com"))
			  (span (@ (class "icon"))
				(i (@ (class "fas fa-envelope")))))
		       (a (@ (class "navbar-item")
			     (id "link-about")
			     (href ,(if post
					"/el#about"
					"#about")))
			  "Ποιοί είμαστε")
		       (a (@ (class "navbar-item")
			     (id "link-news")
			     (href ,(if post
					"/el#news"
					"#news")))
			  "Νέα")
		       (a (@ (class "navbar-item")
			     (id "link-contact")
			     (href ,(if post
					"/el#contact"
					"#contact")))
			  "Επικοινωνία")
                       (a (@ (class ,(if (and (string? post)
                                              (string= post "activities.html"))
                                         "active navbar-item"
                                         "navbar-item"))
			     (id "link-activities")
			     (href "/el/activities.html"))
			  "Δραστηριότητες")
		       (div (@ (class "navbar-item lang-picker has-dropdown is-hoverable"))
			    (a (@ (class "navbar-link"))
			       "EL")
			    (div (@ (class "navbar-dropdown"))
				 (p (@ (class "navbar-item"))
				    "Choose language:")
				 (hr (@ (class "navbar-divider")))
				 (a (@ (class "navbar-item")
				       (href ,(cond
                                               ((string? post) (string-append "/en/" post))
                                               ((list? post) "/en/news")
					       (post (string-append "/en/news/" (post-slug post) ".html"))
					       (else "/en"))))
				    "English")
				 (a (@ (class "navbar-item")
				       (href ,(cond
                                               ((string? post) (string-append "/el/" post))
					       ((list? post) "/el/news")
					       (post (string-append "/el/news/" (post-slug post) ".html"))
					       (else "/el"))))
				    "Ελληνικά"))))))))

(define hero-nav
  `(section (@ (class "hero-nav hero is-medium is-success"))
	    (div (@ (class "hero-head"))
		 (div (@ (class "hero-body"))
		      (div (@ (class "container"))
			   (h1 (@ (class "title logo-title"))
			       "Break the Borders")
			   (h2 (@ (class "subtitle"))
			       "Always Learning"))))))

(define about
  `(section (@ (id "about"))
	    (div (@ (class "container"))
		 (h1 (@ (class "title"))
		     "Ποιοί είμαστε")
		 (p (@ (class "is-size-5"))
		    "Ο οργανισμός Σπάσε τα Σύνορα (Break the Borders) είναι μία αστική μη κερδοσκοπική εταιρεία που δραστηριοποιείται στην υλοποίηση καινοτόμων σχεδίων, στην κινητοποίηση των νέων και στην ανάπτυξη πρωτοβουλίας με στόχο τη δημιουργία ενός δικτύου ενεργών πολιτών στην Ελλάδα, την Ευρώπη και τον κόσμο.")
		 (br)
		 (p (@ (class "is-size-5"))
		    "Ο χαρακτήρας του οργανισμού είναι καθαρά
επιμορφωτικός, πολιτιστικός και μη-κερδοσκοπικός και περιλαμβάνει την
ανάληψη πρωτοβουλίας και δράσης με κεντρικούς άξονες τον πολιτισμό,
την κοινωνική ευθύνη, την αλληλεγγύη και την οικολογία, καθώς και τη
συνεισφορά αυτών στην προσωπική εξέλιξη και βελτίωση του ατόμου ως
πολίτη του κόσμου. Οι δράσεις μας βασίζονται στην εμπειρική μάθηση, τη
μάθηση μέσα από την πράξη (learning-by-doing) και στη μεθοδολογία της
μη τυπικής εκπαίδευσης. Η ΑΜΚΕ Σπάσε τα Σύνορα είναι η διάδοχη της
άρτει διαλυθείσας εταιρείας “Σπάζοντας τα Σύνορα”, με την παρουσία στο
νεοσύστατο οργανισμό σχεδόν όλων των μελών τής πρώτης και τη συνέχεια
σε αυτόν ήδη τρέχοντων προγραμμάτων και συνεργασιών.")
		 (br)
		 (p (@ (class "is-size-5"))
		    "Ο οργανισμός “Σπάζοντας τα Σύνορα” είχε
δραστηριοποιηθεί σε διάφορα πεδία σε τοπικό επίπεδο: συνεργάστηκε με
το “Φάρο Τυφλών” για ένα χρόνο δημιουργώντας το πρόγραμμα “Κάθε πράγμα
στον καιρό του...”, με την “Επάνοδο” για ένα εξάμηνο πρότζεκτ με την
τέχνη ως κύριο εκπαιδευτικό εργαλείο στις φυλακές. Σε ευρωπαϊκό
επίπεδο, υπήρξε ένα από τα ιδρυτικά μέλη του Art in (E)motion και έχει
διοργανώσει και φιλοξενήσει ένα πρόγραμμα ανταλλαγής νέων και μία από
τις πέντε εκπαιδευτικές δράσεις του προγράμματος Grundtvig “Art
in (E)motion, New A.I.M.s for non-formal learning.” Παράλληλα,
συμμετέχει ως συνεργάτης οργανισμός σε ποικίλα ευρωπαϊκά σχέδια
ετησίως και μέσα στο 2018 θα διοργανώσει σε συνεργασία με το ΚΕΑΤ το
πρόγραμμα εθελοντών (EVS) “V-isability”."))))

(define hero-values
  `(section (@ (class "hero-values hero is-warning is-medium has-text-centered"))
	    (div (@ (class "hero-body"))
		 (div (@ (class "container"))
		      (div (@ (class "columns"))
		       (div (@ (class "column is-one-quarter"))
			    (div (@ (class "icon is-large has-text-success"))
				 (i (@ (class "far fa-3x fa-heart"))))
			    (div (@ (class "title"))
				 "Ανθρώπινα δικαιώματα"))
		       (div (@ (class "column is-one-quarter"))
			    (div (@ (class "icon is-large has-text-success"))
				 (i (@ (class "fas fa-3x fa-fist-raised"))))
			    (div (@ (class "title"))
				 "Ενεργοί πολίτες"))
		       (div (@ (class "column is-one-quarter"))
			    (div (@ (class "icon is-large has-text-success"))
				 (i (@ (class "fas fa-3x fa-leaf"))))
			    (div (@ (class "title"))
				 "Οικολογία"))
		       (div (@ (class "column is-one-quarter"))
			    (div (@ (class "icon is-large has-text-success"))
				 (i (@ (class "fas fa-3x fa-hands-helping"))))
			    (div (@ (class "title"))
				 "Αλληλεγγύη")))))))

(define* (news-feed site posts #:optional (n #f))
  `(section (@ (id "news"))
	    (div (@ (class "container"))
		 (h1 (@ (class "title")) "Τα τελευταία νέα μας "
		     (a (@ (href "/el/feed.xml")) (span (@ (class "icon"))
				 (i (@ (class "fas fa-rss"))))))
		 (div (@ (class "columns news-listing"))
		      ,(create-news-feed site posts n))
		 (div (@ (class "columns is-centered")
			 (id "all-news"))
		      (div (@ (class "button"))
			   (a (@ (href "/el/news"))
			      "Όλα τα νέα"))))))

(define* (create-news-feed site posts #:optional (n #f))
  (map (lambda (post)
	 `(div (@ (class "column card is-one-quarter"))
	       (div (@ (class "card-image"))
		    (figure (@ (class "image is-4by3"))
			    (img (@ (src ,(post-ref post 'image))))))
	       ((div (@ (class "card-content"))
		     (h1 (a (@ (class "is-size-5")
			       (href ,(post-uri site post)))
			    ,(post-ref post 'title)))
		     (h2 ,(date->string* (post-date post)))))))
       (if n
	   (take-up-to n (posts/reverse-chronological posts))
	   (posts/reverse-chronological posts))))

(define (index-content site posts)
  `(,(nav) ,hero-nav ,about ,hero-values ,(news-feed site posts 5) ,contact))

(define (index-page site posts)
  (make-page "index.html"
	     (base-tmpl site
			(index-content site posts))
	     sxml->html))



;;; activities page
(define* (hero-nav-activities #:optional title subtitle) 
  `(section (@ (class "hero-nav hero is-small is-success"))
	    (div (@ (class "hero-head"))
		 (div (@ (class "hero-body"))
		      (div (@ (class "container"))
			   (h1 (@ (class "title logo-title"))
			       ,(if title
                                    title
                                    "Δραστηριότητες"))
			  (h2 (@ (class "subtitle"))
			      ,(if subtitle
                                   subtitle
                                   "Εργαλεία ανεπτυγμένα για μια ψηφιακή εποχή")))))))

(define activities-feed
  `(section (@ (class "article-content"))
	    (div (@ (class "container"))
		 (div (@ (class "columns news-listing"))
		      (div (@ (class "column card is-one-third"))
	                   (div (@ (class "card-image"))
		                (figure (@ (class "image is-4by3"))
			                (img (@ (src "/assets/img/step.jpg")
                                                (alt "image of one step forward")))))
	                   ((div (@ (class "card-content"))
		                 (h1 (a (@ (class "is-size-5")
			                   (href "one-step-forward.html"))
			                "Ένα βήμα μπροστά")))))
                      (div (@ (class "column card is-one-third"))
	                   (div (@ (class "card-image"))
		                (figure (@ (class "image is-4by3"))
			                (img (@ (src "/assets/img/i-have.jpg")
                                                (alt "image of one step forward")))))
	                   ((div (@ (class "card-content"))
		                 (h1 (a (@ (class "is-size-5")
			                   (href "i-have.html"))
			                "Έχω/Δεν έχω")))))
                      (div (@ (class "column card is-one-third"))
	                   (div (@ (class "card-image"))
		                (figure (@ (class "image is-4by3"))
			                (img (@ (src "/assets/img/gallery.jpg")
                                                (alt "image of one step forward")))))
	                   ((div (@ (class "card-content"))
		                 (h1 (a (@ (class "is-size-5")
			                   (href "gallery.html"))
			                "Έκθεση φωτογραφίας")))))))))

(define (activities-content site posts)
  `(,(nav "activities.html") ,(hero-nav-activities) ,activities-feed))

(define (activities-page site posts)
  (make-page "activities.html"
             (base-tmpl site
                        (activities-content site posts))
             sxml->html))

;;; one step forward
(define one-step-activity
  `(section (@ (class "article-content")
               (id "activity"))
            (script (@ (src "https://pixijs.download/release/pixi.min.js")))
            (script (@ (src "/assets/js/one-step-forward.js")))
            (div (@ (class "container")
                    (id "main"))
                 (div (@ (class "columns")
                         (id "config"))
                      (div (@ (class "column description"))
                           (p "Ένα ψηφιακό βοήθημα για την πραγματοποίηση της δραστηριότητας Ένα
βήμα μπροστά σε δικτυακές ομάδες. Επίλεξε τον αριθμό των συμμετεχώντων και τους ρόλους που δίνονται
και μοιράσου την οθόνη με τους συμμετέχοντες. Έτσι θα μπορούν να έχουν μια εικόνα για τη
δραστηριότητα. Στο τέλος μπορούν να αποκαλυφθούν οι μυστικοί ρόλοι."))
                      (div (@ (class "column"))
                           (article (@ (class "message is-danger")
                                       (id "warning")
                                       (style "display: none;"))
                                    (div (@ (class "message-header"))
                                         (p "Warning")
                                         (button (@ (class "delete")
                                                    (aria-label "delete")
                                                    (onclick "closeWarning()"))))
                                    (div (@ (class "mesage-body"))
                                         "Ο αριμός των ατόμων πρέπει να είναι ίσος με τους ρόλους."))
                           (form (@ (action "test")
                                    (id "one-step-form"))
		                 (div (@ (class "field"))
		                      (label (@ (class "label")) "Αριθμός ατόμων:")
		                      (div (@ (class "field-body"))
			                   (div (@ (class "field"))
				                (p (@ (class "control"))
				                   (input (@ (class "input")
                                                             (id "number-of-participants")
					                     (type "number")
					                     (name "no_part")))))))
                                 (div (@ (class "field"))
		                      (label (@ (class "label")) "Ρόλοι (χωρισμένοι με κόμμα):")
		                      (div (@ (class "field-body"))
			                   (div (@ (class "field"))
				                (p (@ (class "control"))
				                   (input (@ (class "input")
                                                             (id "number-of-participants")
					                     (type "text")
					                     (name "roles")))))))
		                 (div (@ (class field))
                                      (label (@ (class "label")) "Αριθμός ερωτήσεων:")
                                      (div (@ (class "field-body"))
			                   (div (@ (class "field"))
				                (p (@ (class "control"))
				                   (input (@ (class "input")
					                     (type "number")
					                     (name "no_questions"))))))))
		           (div (@ (class "field"))
		                (div (@ (class "field-body"))
			             (button (@ (class "button is-white")
                                                (id "one-step-start")
                                                (onclick "setup()"))
				             "Ξεκίνα!"))))))))

(define (one-step-content site posts)
  `(,(nav "") ,(hero-nav-activities "One step forward") ,one-step-activity))

(define (one-step-page site posts)
  (make-page "one-step-forward.html"
             (base-tmpl site
                        (one-step-content site posts))
             sxml->html))

;;; i have/i haven't
(define i-have-activity
  `(section (@ (class "article-content")
               (id "activity"))
            (script (@ (src "https://pixijs.download/release/pixi.min.js")))
            (script (@ (src "/assets/js/i-have.js")))
            (div (@ (class "container")
                    (id "main"))
                 (div (@ (class "columns")
                         (id "config"))
                      (div (@ (class "column description"))
                           (p "Ένα ψηφιακό βοήθημα για την πραγματοποίηση της δραστηριότητας Έχω /
Δεν Έχω σε δικτυακές ομάδες. Επίλεξε τον αριθμό των ατόμων και μοιράσου την οθόνη ώστε τα άτομα να
έχουν μια εικόνα της δραστηριότητας."))
                      (div (@ (class "column"))
                           (form (@ (action "test")
                                    (id "i-have-form"))
		                 (div (@ (class "field"))
		                      (label (@ (class "label")) "Αριθμός ατόμων:")
		                      (div (@ (class "field-body"))
			                   (div (@ (class "field"))
				                (p (@ (class "control"))
				                   (input (@ (class "input")
                                                             (id "number-of-participants")
					                     (type "number")
					                     (name "no_part"))))))))
		           (div (@ (class "field"))
		                (div (@ (class "field-body"))
			             (button (@ (class "button is-white")
                                                (id "i-have-start")
                                                (onclick "setup()"))
				             "Ξεκίνα!"))))))))

(define (i-have-content site posts)
  `(,(nav "") ,(hero-nav-activities "I have/I haven't") ,i-have-activity))

(define (i-have-page site posts)
  (make-page "i-have.html"
             (base-tmpl site
                        (i-have-content site posts))
             sxml->html))

;;; gallery activity
(define (gallery-activity)
  `(section (@ (class "article-content")
               (id "activity"))
            (div (@ (class "container")
                    (id "main"))
                 (div (@ (class "columns")
                         (id "config"))
                      (div (@ (class "column description"))
                           (p "Μια έκθεση φωτογραφίας, μια γκαλερί στο διαδίκτυο. Προσπαθεί να
μιμηθεί την αίσθηση μια γκαλερί στον πραγματικό κόσμο, αλλά η επιρροή από τα μέσα κοινωνικής
δικτύωσης είναι αναπόφευκτη."))
                      (div (@ (class "column"))
		           (div (@ (class "field"))
		                (div (@ (class "field-body"))
			             (a (@ (href "/en/gallery1.html"))
                                        (button (@ (class "button is-white"))
				                "Μπες στην Γκαλερί")))))))))

(define (gallery-content site posts)
  `(,(nav "") ,(hero-nav-activities "Gallery") ,(gallery-activity)))

(define (gallery-page site posts)
  (make-page "gallery.html" 
             (base-tmpl site
                        (gallery-content site posts))
             sxml->html))

(site #:title "Break the Borders"
      #:domain "breaktheborders.gr"
      #:make-slug el-post-slug
      #:readers (list skribe-reader html-reader-prime)
      #:builders (list (blog #:prefix "/news"
			     #:theme btb-haunt-theme)
		       index-page
                       activities-page
                       one-step-page
                       i-have-page
                       gallery-page
		       (atom-feed #:blog-prefix "/news")))
